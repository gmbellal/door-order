<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $order_name;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $door_height;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $door_width;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $door_color;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $handle_side;

    /**
     * @ORM\Column(type="integer")
     */
    private $order_by;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;


    /**
     * @ORM\Column(type="string", length=10)
     */
    private $status;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderName(): ?string
    {
        return $this->order_name;
    }

    public function setOrderName(string $order_name): self
    {
        $this->order_name = $order_name;

        return $this;
    }

    public function getDoorHeight(): ?string
    {
        return $this->door_height;
    }

    public function setDoorHeight(string $door_height): self
    {
        $this->door_height = $door_height;

        return $this;
    }

    public function getDoorWidth(): ?string
    {
        return $this->door_width;
    }

    public function setDoorWidth(string $door_width): self
    {
        $this->door_width = $door_width;

        return $this;
    }

    public function getDoorColor(): ?string
    {
        return $this->door_color;
    }

    public function setDoorColor(string $door_color): self
    {
        $this->door_color = $door_color;

        return $this;
    }

    public function getHandleSide(): ?string
    {
        return $this->handle_side;
    }

    public function setHandleSide(string $handle_side): self
    {
        $this->handle_side = $handle_side;

        return $this;
    }

    public function getOrderBy(): ?int
    {
        return $this->order_by;
    }

    public function setOrderBy(int $order_by): self
    {
        $this->order_by = $order_by;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getStatus(): ?string{
        return $this->status;
    }

    public function setStatus(string $status): self{
        $this->status = $status;
        return $this;
    }
}
