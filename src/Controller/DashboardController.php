<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;


class DashboardController extends AbstractController{

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(): Response{
        $stats = [
            "totalUser" => count($this->getDoctrine()->getRepository(User::class)->findAll()),
            "totalOrder" => count($this->getDoctrine()->getRepository(Order::class)->findAll())
        ];
        return $this->render('dashboard/index.html.twig', [
            'title' => "Dashboard :: Admin",
            'stats' => $stats
        ]);
    }


    public function orderList(Request $request){
        $orders = $this->getDoctrine()->getRepository(Order::class)->findAll();
        return $this->render('dashboard/order/index.html.twig', [
            'title' => "Order List :: User",
            'orders' => $orders
        ]);
    }


    
    public function orderDetails($id){
        $order = $this->getDoctrine()->getRepository(Order::class)->findBy(
            ['id' => $id]
        )[0];
        return $this->render('dashboard/order/details.html.twig', [
            'title' => "Order Details :: User",
            'order' => $order
        ]);
    }







}
