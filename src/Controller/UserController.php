<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;



use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;


class UserController extends AbstractController{


    public function __construct(){
        date_default_timezone_set("Asia/Dhaka");
    }

    /**
     * @Route("/user", name="user")
     */
    public function index(): Response{

        return $this->render('user/index.html.twig', [
            'title' => "Dashboard :: User"
        ]);
    }




    public function confirmOrder(Request $request){
        
        $order_name = $request->request->get('order_name');
        $door_height = $request->request->get('door_height');
        $door_width = $request->request->get('door_width');
        $door_color = $request->request->get('door_color');
        $handle_side = $request->request->get('handle_side');

        //return new JsonResponse($door_width);

        if($order_name && $door_height && $door_width && $door_color && $handle_side){
            $Order = new Order();
            $Order->setOrderName($order_name);
            $Order->setDoorHeight($door_height);
            $Order->setDoorWidth($door_width);
            $Order->setDoorColor($door_color);
            $Order->setHandleSide($handle_side);
            $Order->setOrderBy($this->getUser()->getId());
            $Order->setCreatedAt(new \DateTimeImmutable());
            $Order->setStatus("Pending");
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Order);
            $entityManager->flush();
            
            $resp = ['status' => 200, 'message' => 'Order success'];
        }else{
            $resp = ['status' => 400, 'message' => 'Order Failed, Plase Check Input.'];
        }

        return new JsonResponse($resp);
    }



    public function orderList(Request $request){
        $orders = $this->getDoctrine()->getRepository(Order::class)->findBy(
            ['order_by' => $this->getUser()->getId()]
        );
        return $this->render('user/order/index.html.twig', [
            'title' => "Order List :: User",
            'orders' => $orders
        ]);
    }


    
    public function orderDetails($id){
        $order = $this->getDoctrine()->getRepository(Order::class)->findBy(
            ['order_by' => $this->getUser()->getId(), 'id' => $id]
        )[0];
        return $this->render('user/order/details.html.twig', [
            'title' => "Order Details :: User",
            'order' => $order
        ]);
    }






}
